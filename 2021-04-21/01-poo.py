class Perro:

    especie = "Canis lupus familiaris"

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
    
    def descripcion(self):
        return f"{self.nombre} tiene {self.edad} años."

    def ladrar(self, sonido):
        return f"{self.nombre} hace este sonido: {sonido}."

    def __str__(self):
        return f"Especie: {especie}, \nNombre: {self.nombre} \nEdad: {self.edad} años."

class DogoArgentino(Perro):
    raza = "Dogo Argentino"



