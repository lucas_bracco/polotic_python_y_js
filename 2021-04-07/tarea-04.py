#Escribe un programa en Python que muestre la hora actual con una suma de dos horas adicionales.

from datetime import datetime, timedelta

fecha_actual = datetime.now()
hora_actual = datetime.strftime(fecha_actual, "%H:%M:%S")

hora_sumar = timedelta(hours = 2)

fecha_total = fecha_actual + hora_sumar
hora_total = datetime.strftime(fecha_total, "%H:%M:%S")


print("La hora actual es {0}. Dentro de 2 horas serán las {1}".format(hora_actual,hora_total))