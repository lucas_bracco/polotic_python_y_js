#Escribe un programa Python que acepte un número entero (n) y calcule el valor de n + nn + nnn.

num = int(input("Ingrese un número entero: "))
suma = 0

for i in range(1,4):
    suma += (num ** i)
    print(f"ciclo {i} del for suma vale {suma}")

print("El resultado es {0}".format(suma))