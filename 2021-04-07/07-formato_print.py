#Primer ejemplo de formateo de print.
num1 = 13.5
num2 = 4.5
num3 = num1 + num2
print(f"El primer número {num1} + el segundo número {num2} es igual a {num3:0.2f}")
#Segundo ejemplo de formateo de print.
num1 = 5.752342
num2 = 2.2254634
num3 = num1 + num2
print("{0:0.2f} + {1:0.2f} = {2:0.2f}".format(num1,num2,num3))
