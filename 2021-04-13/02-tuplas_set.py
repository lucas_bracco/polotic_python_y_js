#---TUPLA---
punto = (12.5, 13.4)

#---SET---
# Con los set no se pueden repetir elementos.
conjunto =  set()

conjunto.add("Harry")
conjunto.add("Ron")
conjunto.add("Hermione")
conjunto.add("Hermione")

conjunto.remove("Ron")
conjunto.remove("Hermione")
print(conjunto)