#Escribe un programa en Python que acepte una cadena de caracteres y cuente el tamaño de la
#cadena y cuantas veces aparece la letra A (mayuscula y minúscula).

cadena = input("Ingrese un texto: ")
count = 0

# for letra in cadena.lower():
#     if letra == "a":
#         count += 1

count = cadena.lower().count('a')

print("En la cadena: '{0}' hay {1} letras 'a'".format(cadena, count))