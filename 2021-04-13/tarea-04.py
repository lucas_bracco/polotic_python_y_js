#Dada una lista (lista1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]), iterarla y
#mostrar números divisibles por cinco, y si encuentra un número mayor que 150, detenga la iteración
#del bucle.

lista = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]

print("Números multiplo de 5:")
for item in lista:
    if item > 150:
        break
    elif item % 5 == 0:
        print(item)