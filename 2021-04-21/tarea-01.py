# Definicion de las Clases

class Vehiculo():

    def __init__(self, tipo):
        self.tipo = tipo

    def __str__(self):
        return f"{self.tipo}"

class Minibus(Vehiculo):

    def __init__(self, capacidad = 6):
        Vehiculo.__init__(self, "Minibus")
        self.capacidad = capacidad
        self.pasajeros = []

    def asientos_disponibles(self):
        return self.capacidad - len(self.pasajeros)

    def cargar_pasajeros(self, Pasajero):
        if not self.asientos_disponibles():
            return False
        self.pasajeros.append(Pasajero)
        return True

    def __str__(self):
        return f"Vehiculo Minibus de Capacidad {self.capacidad}"

class Pasajeros():

    def __init__(self, dni, nombre):
        self.dni = dni
        self.nombre = nombre

    def __str__(self):
        return f"Persona Nombre: {self.nombre} - DNI: {self.dni}"

# Funciones Adicionales
def buscar_persona(personas, dni):
    for persona in personas:
        if persona.dni == dni:
            return True
    return False

def cargar_personas(personas):
    while len(personas) < 50:
        try:
            dni = int(input("Ingrese el DNI de la persona (0 para terminar): "))
            if dni == 0: break
            if buscar_persona(personas, dni):
                print("La persona ya está cargada")
            else:
                nombre = input("Ingrese el nombre de la persona: ")
                if nombre:
                    pasajero_instancia = Pasajeros(dni, nombre)
                    personas.append(pasajero_instancia)
                else:
                    print("No se ingresó un nombre, intente de nuevo.")
        except ValueError:
            print("Ingreso un valor erroneo.")

# Aplicación
print("Iniciando APP")

minibus1 = Minibus()

lista_personas = []

cargar_personas(lista_personas)

for persona in lista_personas:
    if minibus1.cargar_pasajeros(persona):
        print(f"Se cargó {persona} al {minibus1}")
    else:
        print(f"Ya se superó la capacidad de {minibus1} y no se peuden cargar a {persona}.")

print("FIN APP")