#Escribe un programa en Python que reciba 5 números enteros del usuario y los guarde en una lista.
#Luego recorrer la lista y mostrar los números por pantalla. 

lista = []

for i in range(5):
    num = input("Ingrese un entero: ")
    while not num.isnumeric():
        num = input("El valor ingresado no es un entero.\nIngrese otro: ")
    lista.append(int(num))

print("Valores ingresados por el usuario")
for item in lista:
    print(item, end=" ")