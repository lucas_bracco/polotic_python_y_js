#Escribe un programa Python que acepte el radio de un círculo del usuario y calcule el área. 

from math import pi

def area(r):
    a = pi * (r ** 2)
    return a

radio = float(input("Ingrese el radio del circulo: "))
print("El área del circulo con radio = {0} es {1:0.3f}".format(radio, area(radio))) 
