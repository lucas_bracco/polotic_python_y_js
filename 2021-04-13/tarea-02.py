#Escribe un programa Python que acepte 5 números decimales del usuario.

lista = []

for i in range(5):
    num = input("Ingrese un decimal: ")
    while not num.isnumeric():
        num = input("El valor ingresado no es un decimal.\nIngrese otro: ")
    lista.append(int(num))

print("Valores ingresados por el usuario")
for item in lista:
    print(item, end=" ")
